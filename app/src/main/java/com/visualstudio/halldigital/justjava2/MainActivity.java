package com.visualstudio.halldigital.justjava2;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

/**
 * This app displays an order form to order coffee.
 */
public class MainActivity extends ActionBarActivity {

    int quantity = 1;
    boolean whippedCream = false;
    boolean chocolate = false;
    int quantityUpperBound = 100;
    int quantityLowerBound = 1;
    String userName;

    int priceOfCream = 1;
    int pricePerCup = 5;
    int priceOfChocolate = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * Get the total price, update the display, send an order email
     */
    public void submitOrder(View view) {
        userName = getName();
        int total = calculateTotal();
        display(quantity);
        displayPrice(total);
        sendEmail(total);
    }

    /**
     * Adds the optional toppings onto the (quantity * price per cup)
     *
     * @return Returns an int with the total order price
     */
    private int calculateTotal() {
        int total = quantity * pricePerCup;
        if (whippedCream) {
            total = total + priceOfCream;
        }
        if (chocolate) {
            total = total + priceOfChocolate;
        }
        return total;
    }

    /**
     * Sends an email that includes the calculated total price and order summary.
     * @param price The total price of the complete order
     */
    private void sendEmail(int price) {
        String orderForm;

        orderForm = "Quantity: " + quantity + "\n" + "Chocolate: " + chocolate + "\n" +
                "Cream: " + whippedCream + "\n" + "Name: " + userName + "\n" + "TOTAL: $" + price;

        // Construct the Intent
        Intent intent = new Intent(Intent.ACTION_SENDTO);

        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_SUBJECT, "Order Form");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"googleplex@gmail.com"});
        intent.putExtra(Intent.EXTRA_TEXT, orderForm);

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    /**
     * This method displays the given quantity value on the screen.
     */
    private void display(int number) {
        TextView quantityTextView = (TextView) findViewById(
                R.id.quantity_text_view);
        quantityTextView.setText("" + number);
    }

    /**
     * Decrement order quantity
     */
    public void decrement(View view) {
        if (quantity <= quantityLowerBound) {
            quantity = quantityLowerBound;
        } else {
            quantity--;
        }
        display(quantity);
    }

    /**
     * Increments order quantity
     */
    public void increment(View view) {
        if (quantity >= quantityUpperBound) {
            quantity = quantityUpperBound;
        } else {
            quantity++;
        }
        display(quantity);
    }

    /**
     * This method displays the given price on the screen.
     */
    private void displayPrice(int number) {
        String priceMessage = "Total: $" + (number) + "\nThank you!";
        priceMessage += "\nOrdered for: " + userName;
        displayMessage(priceMessage);
    }

    /**
     * This method displays the given text on the screen.
     */
    private void displayMessage(String message) {
        TextView priceTextView = (TextView) findViewById(R.id.price_text_view);
        priceTextView.setText(message);
    }

    /**
     * Sets the whippedCream boolean to true/false as the checkbox is toggled
     */
    public void whippedCreamClick(View view) {
        whippedCream = ((CheckBox) view).isChecked();
    }

    /**
     * Sets the chocolate boolean to true/false based on checkbox toggle state
     */
    public void chocolateClick(View view) {
        chocolate = ((CheckBox) view).isChecked();
    }

    private String getName() {
        TextView nameField = (TextView) findViewById(R.id.name_field);
        return nameField.getText().toString();
    }
}